const express = require('express');
const app = express();
const cors = require('cors');
require('dotenv').config({ path: './config/config.env' });
const connectDb = require('./src/database/db');
const server = require('http').createServer(app);
const logRouter = require('./src/controller/log');
const contactRouter = require('./src/controller/contact');
var path = require('path');

connectDb();
const PORT = process.env.PORT;
app.use('/', express.static(__dirname + '/public'));
app.use(express.json());
app.use(cors());

app.use(contactRouter);
app.use(logRouter);

app.get('/', (req, res) => {
	res.sendFile(path.join(__dirname + '/public/index.html'));
});

server.listen(PORT, () => {
	try {
		console.log(`server listening on port ${PORT}`);
	} catch (error) {
		console.log(error.message);
	}
});
