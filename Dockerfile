FROM node:16.13.2-alpine

WORKDIR /usr/src

COPY package*.json ./
COPY yarn.lock ./

RUN yarn  && yarn cache clean --mirror

COPY . ./

EXPOSE 8080

CMD [ "node", "src/index.js" ]